package Isaac;

/**
 * Clase personaje con su constructor y las funciones de movimiento, colision
 * con enemigo y la deteccion de que acabade disparar.
 * 
 * @author DaniGarcia
 *
 */
public class Buenos extends Personajes {

	char direccion = 'q';
	int cooldown = 20;

	public Buenos(String name, int x1, int y1, int x2, int y2, String path, int vida, int damage, int vel) {
		super(name, x1, y1, x2, y2, path, vida, damage, vel);
	}

	private Buenos(String path) {
		super("Isaac", 875, 400, 990, 525, "Alex.png", 3, 1, 8);
	}
	

	/**
	 * Movimiento hacia la izquierda
	 * 
	 * @param f pasas el campo de pixeles
	 */
	public void moveIzq(Field f) {

		x1 -= this.vel;
		x2 -= this.vel;
		if (firstCollidesWithList(Mapa.muro) != null) {
			colision(f);
			x1 += this.vel;
			x2 += this.vel;
		}

		direccion = 'a';
	}

	/**
	 * Movimiento hacia la derecha
	 * 
	 * @param f pasas el campo de pixeles
	 */
	public void moveDer(Field f) {
		// TODO Auto-generated method stub
		x1 += this.vel;
		x2 += this.vel;
		if (firstCollidesWithList(Mapa.muro) != null) {
			colision(f);
			x1 -= this.vel;
			x2 -= this.vel;
		}

		direccion = 'd';

	}

	/**
	 * Movimiento hacia arriba
	 * 
	 * @param f pasas el campo de pixeles
	 */
	public void moveArr(Field f) {
		// TODO Auto-generated method stub
		y1 -= this.vel;
		y2 -= this.vel;
		if (firstCollidesWithList(Mapa.muro) != null) {
			colision(f);
			y1 += this.vel;
			y2 += this.vel;
		}

		direccion = 'w';
	}

	/**
	 * Movimiento hacia abajo
	 * 
	 * @param f pasas el campo de pixeles
	 */
	public void moveAba(Field f) {
		// TODO Auto-generated method stub
		y1 += this.vel;
		y2 += this.vel;
		if (firstCollidesWithList(Mapa.muro) != null) {
			colision(f);
			y1 -= this.vel;
			y2 -= this.vel;
		}

		direccion = 's';

	}

	/**
	 * Colision contra un enemigo
	 * 
	 * @param f pasas el campo de pixeles
	 */
	private void colision(Field f) {
		Sprite s = firstCollidesWithField(f);
		// si ha colisionado con algo
		if (s != null) {
			// si s es de Clase Enemigo.
			if (s instanceof Enemigo) {
				// casteo de enemigo
				Enemigo en = (Enemigo) s;
				mePegaste();
			}

		}

	}

	/**
	 * Al gcolisionar un enemigo el personaje se apartara de su enemigo y perdera
	 * vida
	 * 
	 * @return
	 * 
	 */
	public void mePegaste() {
		// TODO Auto-generated method stub
		this.vida -= 1;
		ProyectoIsaac.w.playSFX("golpe.wav");
		System.out.println("AAAAH");
		if (direccion == 'a') {
			x1 += 100;
			x2 += 100;
		}
		if (direccion == 'd') {
			x1 -= 100;
			x2 -= 100;
		}
		if (direccion == 'w') {
			y1 += 100;
			y2 += 100;
		}
		if (direccion == 's') {
			y1 -= 100;
			y2 -= 100;
		}

	}

	/**
	 * Genera el proyectil que se acaba de disparar
	 * 
	 * @return p devuelve el proyectil creado en caso que se pueda disparar o un
	 *         null en caso de que este en cooldown
	 */
	public Proyectil shoot() {
		// TODO Auto-generated method stub
		if (cooldown == 20) {
			cooldown = 0;
			Proyectil p = new Proyectil("Proyectil", ((x1 + x2) / 2) - 25, ((y1 + y2) / 2) - 25, ((x1 + x2) / 2) + 25,
					((y1 + y2) / 2) + 25, "tear.png", direccion);
			return p;
		} else {
			return null;
		}

	}

	/**
	 * Actualiza el tipo transcurrido desde el ultimo proyectil que ha sido lanzado
	 */
	public void update() {
		if (cooldown < 20)
			cooldown++;
	}

	static Buenos p = null;

	public static Buenos getIsaac() {
		if (p == null) {
			p = new Buenos("Isaac");
			return p;
			// si no es null, es que ja s'ha cridat abans
		} else {
			return p;
		}

	}

}
