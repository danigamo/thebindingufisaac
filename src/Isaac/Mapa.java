package Isaac;

import java.util.ArrayList;

public class Mapa {

	static ArrayList<Puertas> puertas = new ArrayList<>(); // lista de puertas cerradas
	static ArrayList<Muros> muro = new ArrayList<>(); // lista de muros para tener un lugar de juego
	static ArrayList<DeteccionesIDK> deteccion = new ArrayList<>();
	static ArrayList<Vidas> vidas = new ArrayList<>(); // lista para poner los sprites de la vida actual del jugador
	static Vidas corazones = new Vidas("corason", 50, 50, 300, 100, "3cor.png");
	
	// generamos los muros
	public static void sala0() {
		muro.clear();
		muro.add(new Muros("arriba1", 0, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 400, ""));
		muro.add(new Muros("izquierda2", 0, 575, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 850, 1000, ""));
		muro.add(new Muros("abajo2", 1040, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 400, ""));
		muro.add(new Muros("derecha2", 1680, 575, 1925, 1000, ""));
		
	}

	public static void sala1() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 400, ""));
		muro.add(new Muros("derecha2", 1680, 575, 1925, 1000, ""));


	}

	public static void sala2() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 400, ""));
		muro.add(new Muros("izquierda2", 0, 575, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 1000, ""));

	}

	public static void sala3() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 850, 105, ""));
		muro.add(new Muros("arriba2", 1040, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 850, 1000, ""));
		muro.add(new Muros("abajo2", 1040, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 1000, ""));

	}

	public static void sala4() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 850, 105, ""));
		muro.add(new Muros("arriba2", 1040, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 400, ""));
		muro.add(new Muros("derecha2", 1680, 575, 1925, 1000, ""));

	}

	public static void sala5() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 400, ""));
		muro.add(new Muros("izquierda2", 0, 575, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));
		
		muro.add(new Muros("derecha1", 1680, 0, 1925, 400, ""));
		muro.add(new Muros("derecha2", 1680, 575, 1925, 1000, ""));

	}
	
	public static void sala6() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 850, 105, ""));
		muro.add(new Muros("arriba2", 1040, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 400, ""));
		muro.add(new Muros("izquierda2", 0, 575, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));
		
		muro.add(new Muros("derecha1", 1680, 0, 1925, 1000, ""));

	}
	
	public static void salaBoss() {
		muro.clear();

		muro.add(new Muros("arriba1", 0, 0, 1925, 105, ""));

		muro.add(new Muros("izquierda1", 0, 0, 215, 1000, ""));

		muro.add(new Muros("abajo1", 0, 800, 1925, 1000, ""));

		muro.add(new Muros("derecha1", 1680, 0, 1925, 1000, ""));

	}
	
	
}
//separados
//
//Mapa.tierras.add(new Terreno("arriba1", 0, 0, 850, 105, ""));
//Mapa.tierras.add(new Terreno("arriba2", 1040, 0, 1925, 105, ""));
//
//Mapa.tierras.add(new Terreno("izquierda1", 0, 0, 215, 400, ""));
//Mapa.tierras.add(new Terreno("izquierda2", 0, 575, 215, 1000, ""));
//
//Mapa.tierras.add(new Terreno("abajo1", 0, 800, 850, 1000, ""));
//Mapa.tierras.add(new Terreno("abajo2", 1040, 800, 1925, 1000, ""));
//
//Mapa.tierras.add(new Terreno("derecha1", 1680, 0, 1925, 400, ""));
//Mapa.tierras.add(new Terreno("derecha2", 1680, 575, 1925, 1000, ""));
//
//junto
//
//tierras.add(new Terreno("arriba1", 0, 0, 1925, 105, ""));
//
//tierras.add(new Terreno("izquierda1", 0, 0, 215, 1000, ""));
//
//tierras.add(new Terreno("abajo1", 0, 800, 1925, 1000, ""));
//
//tierras.add(new Terreno("derecha1", 1680, 0, 1925, 1000, ""));
