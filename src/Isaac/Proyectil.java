package Isaac;

import Isaac.Buenos;
import Isaac.Enemigo;
import Isaac.Sprite;

/**
 * Clase proyectil con su constructor y las funciones de movimieto segun la direccion en la que mira nuestro personaje y la colision contra lo enemigos
 * 
 * @author DaniGarcia
 *
 */
public class Proyectil extends Sprite {

	char direccion;

	public Proyectil(String name, int x1, int y1, int x2, int y2, String path, char dir) {
		super(name, x1, y1, x2, y2, path);
		direccion = dir;
	}

	/**
	 * Movimiento que tiene el proyectil basado en la direccion que mira nuestro personaje
	 * @param f pasas un campo de pixeles
	 */
	public void move(Field f) {
		if (direccion == 'a') {
			x1 -= 8;
			x2 -= 8;
		}
		if (direccion == 'd') {
			x1 += 8;
			x2 += 8;
		}
		if (direccion == 'w') {
			y1 -= 8;
			y2 -= 8;
		}
		if (direccion == 's') {
			y1 += 8;
			y2 += 8;
		}
		colision(f);
	}
	
	/**
	 * Colision del proyectil contra un enemigo
	 * @param f pasas un campo de pixeles
	 */
	private void colision(Field f) {
		Sprite s = firstCollidesWithField(f);
		// si ha colisionado con algo
		if (s != null) {
			// si s es de Clase Enemigo.
			if (s instanceof Enemigo) {
				// casteo de enemigo
				Enemigo en = (Enemigo) s;
				en.golpear();
				delete();
			}
			if (!(s instanceof Buenos)) {
				delete();
			}
		}

	}
}
