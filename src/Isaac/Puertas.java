package Isaac;

/**
 * Clase puertas que contiene el constructor de puertas que nos impediran el
 * paso a la siguiente sala
 * 
 * @author DaniGarcia
 *
 */
public class Puertas extends Sprite {

	public Puertas(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);

	}

}
