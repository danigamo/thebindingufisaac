package Isaac;

import java.util.Random;

/**
 * Clase enemigo que contiene su constructor y las funciones de movimiento y de
 * ser golpeado
 * 
 * @author DaniGarcia
 *
 */
public class Enemigo extends Personajes {

	public Enemigo(String name, int x1, int y1, int x2, int y2, String path, int vida, int damage, int vel) {
		super(name, x1, y1, x2, y2, path, vida, damage, vel);
	}
	
	public Enemigo(String path) {
		super("enemi", 0, 0, 0, 0, "enemigo.png", 3, 1, 2);
	}

	boolean dx; /// true izq false der
	boolean dy; /// true arriba false abajo

	int cd;

	public void move(Field f) {
		cd++;

		if (cd == 50) {
			cd = 0;
			Random r = new Random();
			int random = r.nextInt(2);
			switch (random) {
			case 0:
				dx = !dx;
				break;
			case 1:
				dy = !dy;
				break;
			}
		}
		if (dx) {
			x1 -= 2;
			x2 -= 2;
			if (firstCollidesWithField(f) != null) {
				colision(f);
				x1 += 2;
				x2 += 2;
			}
		}
		if (dy) {
			y1 -= 2;
			y2 -= 2;
			if (firstCollidesWithField(f) != null) {
				colision(f);
				y1 += 2;
				y2 += 2;
			}
		}
		if (!dx) {
			x1 += 2;
			x2 += 2;
			if (firstCollidesWithField(f) != null) {
				colision(f);
				x1 -= 2;
				x2 -= 2;
			}

		}
		if (!dy) {

			y1 += 2;
			y2 += 2;
			if (firstCollidesWithField(f) != null) {
				colision(f);
				y1 -= 2;
				y2 -= 2;
			}
		}

	}

	private void colision(Field f) {
		Sprite s = firstCollidesWithField(f);
		// si ha colisionado con algo
		if (s != null) {
			// si s es de Clase Personaje.
			if (s instanceof Buenos) {
				// casteo de enemigo
				Buenos p = (Buenos) s;
				p.mePegaste();
			}

		}
	}

	/**
	 * En el momento en que un proyectil colisiona contra un enemigo este pierde
	 * vida y al llegar a 0 muere
	 */
	public void golpear() {
		this.vida -= 1;
		if (vida == 0) {
			delete();
			ProyectoIsaac.contene--;
			ProyectoIsaac.score+=100;
		}
		System.out.println("ouch" + vida);
		System.out.println(ProyectoIsaac.contene);
	}

}