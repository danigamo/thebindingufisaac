package Isaac;
 /**
  * Clase vidas que contiene el constructor para los spites de la vida del jugador
  * 
  * @author DaniGarcia
  *
  */
public class Vidas extends Sprite {

	public Vidas(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);

	}

}
