package Isaac;
 /**
  * Clase texto que contiene el constructor para el texto de las imagenes del final
  * 
  * @author DaniGarcia
  *
  */
public abstract class Textos extends Sprite {

	public Textos(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
		this.text = true;
	}

}
