package Isaac;

public class Personajes extends Sprite{

	int vida;
	int damage;
	int vel;
	
	public Personajes(String name, int x1, int y1, int x2, int y2, String path, int vida, int damage, int vel) {
		super(name, x1, y1, x2, y2, path);
		this.vida = vida;
		this.damage = damage;
		this.vel = vel;
	}
	
}
