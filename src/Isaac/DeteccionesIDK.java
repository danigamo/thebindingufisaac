package Isaac;
 /**
  * Clase detecciones que contiene el constructor para muros invisibles que permitiran el cambio de sala de nuestro personaje
  * 
  * @author DaniGarcia
  *
  */
public class DeteccionesIDK extends Sprite{

	public DeteccionesIDK(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		
	}

	
}
