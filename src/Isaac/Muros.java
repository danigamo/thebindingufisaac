package Isaac;

/**
 * Clase terreno que contiene el constructor de los muros que limitan la zona de juego para asi no dejar que nuestro personaje o proyectil salga de la pantalla
 * 
 * @author DaniGarcia
 *
 */
public class Muros extends Sprite{

	public Muros(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		
		terrain = false;
	}
	
	

}
