package Isaac;

import java.util.ArrayList;
import java.util.Random;

public class Salas {

	static String salasActual = "s0";
	static Random r = new Random();


	public static void Planta1() {

//		System.out.println(salasActual);

		// Si estas en sala 0
		if (salasActual.equals("s0")) {
			ProyectoIsaac.f.background = "sala0.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.izq0)) {
				Buenos.getIsaac().x1 = 1550;
				Buenos.getIsaac().x2 = 1665;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s1";
				Mapa.sala1();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.der0)) {
				Buenos.getIsaac().x1 = 220;
				Buenos.getIsaac().x2 = 335;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s2";
				Mapa.sala2();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.abajo0)) {
				Buenos.getIsaac().x1 = 875;
				Buenos.getIsaac().x2 = 990;
				Buenos.getIsaac().y1 = 180;
				Buenos.getIsaac().y2 = 305;
				salasActual = "s3";
				Mapa.sala3();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
		}
		// si estas en sala 1
		if (salasActual.equals("s1")) {
			ProyectoIsaac.f.background = "sala1.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.der0)) {
				Buenos.getIsaac().x1 = 220;
				Buenos.getIsaac().x2 = 335;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s0";
				Mapa.sala0();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			
		}
		// si estas en sala 2
		if (salasActual.equals("s2")) {
			ProyectoIsaac.f.background = "sala2.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.izq0)) {
				Buenos.getIsaac().x1 = 1550;
				Buenos.getIsaac().x2 = 1665;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s0";
				Mapa.sala0();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
		}
		// si estas en sala3
		if (salasActual.equals("s3")) {
			ProyectoIsaac.f.background = "sala3.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.arriba0)) {
				Buenos.getIsaac().x1 = 875;
				Buenos.getIsaac().x2 = 990;
				Buenos.getIsaac().y1 = 665;
				Buenos.getIsaac().y2 = 790;
				salasActual = "s0";
				Mapa.sala0();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.abajo0)) {
				Buenos.getIsaac().x1 = 875;
				Buenos.getIsaac().x2 = 990;
				Buenos.getIsaac().y1 = 180;
				Buenos.getIsaac().y2 = 305;
				salasActual = "s4";
				Mapa.sala4();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
		}
		// si estas en sala 4
		if (salasActual.equals("s4")) {
			ProyectoIsaac.f.background = "sala4.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.arriba0)) {
				Buenos.getIsaac().x1 = 875;
				Buenos.getIsaac().x2 = 1000;
				Buenos.getIsaac().y1 = 665;
				Buenos.getIsaac().y2 = 790;
				salasActual = "s3";
				Mapa.sala3();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.der0)) {
				Buenos.getIsaac().x1 = 220;
				Buenos.getIsaac().x2 = 335;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s5";
				Mapa.sala5();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}

		}
		// si estas en sala5
		if (salasActual.equals("s5")) {
			ProyectoIsaac.f.background = "sala5.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.der0)) {
				Buenos.getIsaac().x1 = 220;
				Buenos.getIsaac().x2 = 335;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s6";
				Mapa.sala6();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.izq0)) {
				Buenos.getIsaac().x1 = 1550;
				Buenos.getIsaac().x2 = 1665;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s4";
				Mapa.sala4();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
		}
		// si estas en sala6
		if (salasActual.equals("s6")) {
			ProyectoIsaac.f.background = "sala6.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.izq0)) {
				Buenos.getIsaac().x1 = 1550;
				Buenos.getIsaac().x2 = 1665;
				Buenos.getIsaac().y1 = 410;
				Buenos.getIsaac().y2 = 535;
				salasActual = "s5";
				Mapa.sala5();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.arriba0)) {
				Buenos.getIsaac().x1 = 875;
				Buenos.getIsaac().x2 = 1000;
				Buenos.getIsaac().y1 = 665;
				Buenos.getIsaac().y2 = 790;
				salasActual = "sb";
				Mapa.salaBoss();
				ProyectoIsaac.enemigos.clear();
				ProyectoIsaac.tiros.clear();
			}
		}
		// si estas en sala7
		if (salasActual.equals("sb")) {
			ProyectoIsaac.f.background = "salaBoss.png";
			// salida
			if (Buenos.getIsaac().collidesWith(ProyectoIsaac.arriba0)) {
			
			}
		}		
	}

}
//private static void cambiadesala() {
//
//	if (per.collidesWith(izq0)) {
//		Buenos.getIsaac().x1 = 1550;
//		Buenos.getIsaac().x2 = 1665;
//		Buenos.getIsaac().y1 = 410;
//		Buenos.getIsaac().y2 = 535;
//		salas = true;
//		enemigos.clear();
//		tiros.clear();
//
//	}
//	if (per.collidesWith(der0)) {
//		Buenos.getIsaac().x1 = 220;
//		Buenos.getIsaac().x2 = 335;
//		Buenos.getIsaac().y1 = 410;
//		Buenos.getIsaac().y2 = 535;
//		salas = true;
//		enemigos.clear();
//		tiros.clear();
//
//	}
//	if (per.collidesWith(abajo0)) {
//		Buenos.getIsaac().x1 = 875;
//		Buenos.getIsaac().x2 = 990;
//		Buenos.getIsaac().y1 = 180;
//		Buenos.getIsaac().y2 = 305;
//		salas = true;
//		enemigos.clear();
//		tiros.clear();
//
//	}
//	if (per.collidesWith(arriba0)) {
//		Buenos.getIsaac().x1 = 875;
//		Buenos.getIsaac().x2 = 990;
//		Buenos.getIsaac().y1 = 665;
//		Buenos.getIsaac().y2 = 790;
//		salas = true;
//		enemigos.clear();
//		tiros.clear();
//
//	}
//
//}