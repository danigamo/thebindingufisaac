package Isaac;

import java.awt.Container;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IllegalFormatCodePointException;
import java.util.Iterator;
import java.util.Random;

import Isaac.Muros;
import Isaac.Sprite;
import Isaac.Ranking;

/*TODO
--Maybe Musica pipo
--cambiar sprites
 */
/**
 * Version cutre del juego The Binding of Isaac. Consta de un personaje el cual
 * se mueve por salas en las que apareceran enemigos que tendras que eliminar
 * para poder continuar. El juego acaba cuando mueres o te cansas y pulsas la
 * letra 'p'. Al acabar aparecera una imagen segun los enemigos que hayas
 * matado, victoria o derrota.
 * 
 * @author DaniGarcia
 * @version 1.0
 *
 */
public class ProyectoIsaac {
	static Random r = new Random();
	static Field f = new Field();
	static Window w = new Window(f);
	static int contene = 0; // contador de enemigos
	static int score = 0; // contador de enemigos que has matado
	static boolean finPartida = false; // fin de la partida, acaba el bucle principal
//	static Buenos per = Buenos.getIsaac();
	static ArrayList<Proyectil> tiros = new ArrayList<>(); // lista de proyectiles
	static ArrayList<Enemigo> enemigos = new ArrayList<>(); // lista de enemigos
	static ArrayList<Ranking> ranking = new ArrayList<Ranking>();
	static ArrayList<Objeto> obj = new ArrayList<Objeto>();
	static ArrayList<DeteccionesIDK> deteccion = new ArrayList<>(); // lista de muros invisibles que haran que nuestro
																	// personaje "cambie" de sala
	static DeteccionesIDK arriba0 = new DeteccionesIDK("deteccionAr", 850, 30, 1040, 80, "");
	static DeteccionesIDK abajo0 = new DeteccionesIDK("deteccionAb", 850, 880, 1040, 930, "");
	static DeteccionesIDK izq0 = new DeteccionesIDK("deteccionIz", 50, 400, 100, 575, "");
	static DeteccionesIDK der0 = new DeteccionesIDK("deteccionDe", 1800, 400, 1850, 575, "");

	static Texto r1 = new Texto("ranking", 670, 152, 1174, 282, "");
	static Texto r2 = new Texto("ranking", 670, 312, 1174, 442, "");
	static Texto r3 = new Texto("ranking", 670, 472, 1174, 602, "");

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {

		w.playMusic("pipoSong.wav");

		Mapa.muro.add(new Muros("arriba1", 0, 0, 850, 105, ""));
		Mapa.muro.add(new Muros("arriba2", 1040, 0, 1925, 105, ""));

		Mapa.muro.add(new Muros("izquierda1", 0, 0, 215, 400, ""));
		Mapa.muro.add(new Muros("izquierda2", 0, 575, 215, 1000, ""));

		Mapa.muro.add(new Muros("abajo1", 0, 800, 850, 1000, ""));
		Mapa.muro.add(new Muros("abajo2", 1040, 800, 1925, 1000, ""));

		Mapa.muro.add(new Muros("derecha1", 1680, 0, 1925, 400, ""));
		Mapa.muro.add(new Muros("derecha2", 1680, 575, 1925, 1000, ""));

		while (true) {
		
			cargarRankin();
			start();
			System.out.println(Buenos.getIsaac().vida);
		}

	}

	private static void jugar() throws InterruptedException, IOException, ClassNotFoundException {

		carga();
		boolean init = false;
		finPartida = false;
		reset();
		while (!finPartida) {
			Buenos.getIsaac();
			init = input(init);
			Buenos.getIsaac().update();
			for (Proyectil p : tiros) {
				p.move(f);
			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.addAll(Mapa.muro);
			sprites.add(Buenos.getIsaac());
			sprites.addAll(enemigos);
			sprites.addAll(tiros);
			sprites.add(Mapa.corazones);
			sprites.add(arriba0);
			sprites.add(abajo0);
			sprites.add(der0);
			sprites.add(izq0);
			sprites.addAll(obj);
			sprites.addAll(Mapa.puertas);
			for (Iterator iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}

			if (Buenos.getIsaac().collidesWith(arriba0) || Buenos.getIsaac().collidesWith(abajo0)
					|| Buenos.getIsaac().collidesWith(izq0) || Buenos.getIsaac().collidesWith(der0)) {
				init = true;
				contene = 0;
			}

			if (contene == 0) {
				sprites.removeAll(Mapa.puertas);
			}

			if (Buenos.getIsaac().vida == 2) {
				Mapa.corazones.changeImage("2cor.png");
			}
			if (Buenos.getIsaac().vida == 1) {
				Mapa.corazones.changeImage("1cor.png");
			}
			if (Buenos.getIsaac().vida == 0) {
				sprites.clear();
				finPartida = true;
				gameOver();
			}

			if (w.getPressedKeys().contains('p')) {
				menuPausa();
				break;
			}

			f.draw(sprites);
			Thread.sleep(20);

		}
		guardarRankin();
	}

	private static void carga() {
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		sprites.clear();
		f.background = "load.jpg";
		f.draw(sprites);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void reset() {
		Buenos.getIsaac().x1 = 875;
		Buenos.getIsaac().y1 = 400;
		Buenos.getIsaac().x2 = 990;
		Buenos.getIsaac().y2 = 525;
		Buenos.getIsaac().vida = 3;
		Mapa.corazones.changeImage("3cor.png");
		contene = 0;
		Mapa.sala0();
		Salas.salasActual = "s0";
		f.background = "Sala0.png";
		tiros.clear();
		enemigos.clear();
	}

	private static void menuPausa() throws ClassNotFoundException, IOException, InterruptedException {
		boolean jugar = false;
		Sprite guardar = new Sprite("Guardar", 800, 550, 1104, 650, "save.png");
		Sprite continuar = new Sprite("Continue", 800, 660, 1104, 710, "resume.png");
		Sprite salir = new Sprite("Salir", 800, 720, 1104, 770, "exitPausa.png");
		while (!jugar) {
//			w.changeSize(1925, 1000);

			f.background = "menuPausa.png";

			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			ArrayList<Sprite> sp = new ArrayList<Sprite>();
			sp.add(guardar);
			sp.add(salir);
			sp.add(continuar);
			f.draw(sp);
			for (Sprite s : sp) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "save.png") {
						guardar();
						jugar = true;
					}
					if (s.path == "exitPausa.png") {
						jugar = true;
						finPartida = true;
						System.out.println(finPartida);
					}
					if (s.path == "resume.png") {
						cargar();
						jugar = true;
					}

				}
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private static void cargar() throws IOException, ClassNotFoundException, InterruptedException {
		File f1 = new File("guardado/guardar.txt");
		if (f1.exists()) {
			FileInputStream fis = new FileInputStream(f1);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Buenos.p = (Buenos) ois.readObject();

//			Mapa.vidas = (ArrayList<Vidas>) ois.readObject();

			jugar();
		}
	}

	public static void guardar() throws IOException {
		File f = new File("guardado/guardar.txt");
		if (f.exists())
			f.delete();
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(Buenos.getIsaac());

		oos.writeObject(Mapa.corazones);

		oos.flush();
		oos.close();
	}

	private static void guardarRankin() throws IOException {
		System.out.println("Escribe tu nombre");
		String nombre = w.showInputPopup("Escribre tu nombre");
		ranking.add(new Ranking(nombre, score));

		File f2 = new File("ranking/RankingLeer.txt");
		FileWriter out = new FileWriter(f2);
		BufferedWriter bw = new BufferedWriter(out);
		f2.createNewFile();
		Collections.sort(ranking);
		for (Ranking ranking : ranking) {
			bw.write(ranking.nombre + "," + ranking.puntos + "," + ranking.fecha);
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}

	private static void cargarRankin() throws IOException {
		File f = new File("ranking/RankingLeer.txt");
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);

		while (br.ready()) {
			String s = br.readLine();
			String[] spl = s.split(",");
			Ranking r = new Ranking("f", 0);
			r.nombre = spl[0];
			r.puntos = Integer.parseInt(spl[1]);
			r.fecha = spl[2];
			ranking.add(r);
		}
		br.close();
	}

	public static void generaenem() {

		if (Salas.salasActual.equals("sb")) {
			enemigoBoss bs = new enemigoBoss("boss.png");
			contene++;
		} else {
			int nenem = r.nextInt(3) + 1;

			for (int i = 0; i < nenem; i++) {
				enemigoNormal e2 = new enemigoNormal("enemigo.png");
				contene++;
			}
		}

	}

	private static void moveenem() {
		for (Enemigo e : enemigos) {
			e.move(f);
		}
	}

	private static boolean input(boolean init) throws ClassNotFoundException, InterruptedException, IOException {
		generarsala();
		movimiento();
		moveenem();
		if (init) {
			generapuertas();
			generaenem();
			init = false;
		}
		recompensa();

		return init;
	}

	private static void recompensa() throws ClassNotFoundException, InterruptedException, IOException {
		if (Salas.salasActual.equals("sb")) {
			if (contene == 0) {
				Objeto god = new Objeto("trap", 875, 300, 975, 400, "trap.jpg");
				obj.add(god);
				if (Buenos.getIsaac().collidesWith(god)) {
					obj.clear();
					score += 500;
					pasarLvl();
				}
			}
		}

	}

	private static void pasarLvl() throws ClassNotFoundException, InterruptedException, IOException {
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		sprites.clear();
		f.background = "carga.png";
		f.draw(sprites);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		contene = 0;
		Mapa.sala0();
		Salas.salasActual = "s0";
		f.background = "Sala0.png";
		tiros.clear();
		enemigos.clear();
		Buenos.getIsaac().x1 = 875;
		Buenos.getIsaac().y1 = 400;
		Buenos.getIsaac().x2 = 990;
		Buenos.getIsaac().y2 = 525;
	}

	private static void generarsala() {

		Salas.Planta1();

	}

	private static void generapuertas() {
		Mapa.puertas.add(new Puertas("puertaAr", 850, 60, 1040, 180, "puerta.png"));
		Mapa.puertas.add(new Puertas("puertaAb", 850, 780, 1040, 910, "puertaabajo.png"));
		Mapa.puertas.add(new Puertas("puertaIz", 90, 400, 220, 575, "puertaizq.png"));
		Mapa.puertas.add(new Puertas("puertaDer", 1675, 400, 1800, 575, "puertader.png"));
	}

	/**
	 * Segun la tecla pulsada el movimiento de nuestro personaje cambiara su
	 * direccion. La teclas de movimiento son WASD
	 * 
	 */
	private static void movimiento() {

		if (w.getPressedKeys().contains('a')) {
			Buenos.getIsaac().moveIzq(f);
		}

		if (w.getPressedKeys().contains('d')) {
			Buenos.getIsaac().moveDer(f);
		}

		if (w.getPressedKeys().contains('w')) {
			Buenos.getIsaac().moveArr(f);
		}

		if (w.getPressedKeys().contains('s')) {
			Buenos.getIsaac().moveAba(f);
		}

		if (w.getPressedKeys().contains(' ')) {
			Proyectil p = (Buenos.getIsaac().shoot());
			if (p != null) {
				tiros.add(p);
			}

		}

	}

	private static void start() throws ClassNotFoundException, IOException, InterruptedException {
		boolean start = false;
		int x = 0;
		int y = 0;

		while (!start) {

			f.background = "granMenu1.png";

			x = f.getMouseX();
			y = f.getMouseY();

			ArrayList<Sprite> sprites = new ArrayList<>();
			Opciones b5 = new Opciones(775, 730, 1050, 830, "play.png");
			sprites.add(b5);
			f.draw(sprites);

			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "play.png") {
						menu();
						start = true;
					}
				}
			}

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void menu() throws ClassNotFoundException, IOException, InterruptedException {
		boolean jugar = false;
		int x = 0;
		int y = 0;

		while (!jugar) {
			// Fondo del menu.
			f.background = "menu.png";

			// Uso del raton en el menu.

			x = f.getMouseX();
			y = f.getMouseY();

			// Sprites del menu.
			ArrayList<Sprite> sprites = new ArrayList<>();
			Opciones b = new Opciones(675, 175, 1125, 275, "jugar.png");
			Opciones b2 = new Opciones(680, 280, 1130, 380, "continuar.png");
			Opciones b3 = new Opciones(725, 385, 1100, 485, "stats.png");
			Opciones b4 = new Opciones(690, 490, 1140, 590, "opciones.png");
			Opciones b5 = new Opciones(775, 595, 1050, 695, "exit.png");
			sprites.add(b);
			sprites.add(b2);
			sprites.add(b3);
			sprites.add(b4);
			sprites.add(b5);

			f.draw(sprites);

			// Colision del raton con el sprite correspondiente segun su path.
			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "jugar.png") {
						jugar();
						jugar = true;
					}
					if (s.path == "continuar.png") {
						cargar();
						jugar = true;
					}
					if (s.path == "stats.png") {
						rankin();
						jugar = true;
					}
					if (s.path == "opciones.png") {
						opciones();
						jugar = true;
					}
					if (s.path == "exit.png") {
						System.exit(0);
					}
				}
			}

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void rankin() throws ClassNotFoundException, IOException, InterruptedException {
		boolean rankin = false;
		int x = 0;
		int y = 0;

		while (!rankin) {

			f.background = "menu2.png";

			Font fuente = new Font("Monospaced", Font.ROMAN_BASELINE, 50);
			String puntos = ranking.get(0).nombre + ": " + ranking.get(0).puntos;
			String puntos2 = ranking.get(1).nombre + ": " + ranking.get(1).puntos;
			String puntos3 = ranking.get(2).nombre + ": " + ranking.get(2).puntos;
			r1.font = fuente;
			r2.font = fuente;
			r3.font = fuente;
			r1.path = puntos;
			r2.path = puntos2;
			r3.path = puntos3;

			// Uso del raton en el menu.

			x = f.getMouseX();
			y = f.getMouseY();

			ArrayList<Sprite> sprites = new ArrayList<>();
			Opciones b5 = new Opciones(1450, 175, 1700, 300, "back.png");
			sprites.add(b5);
			sprites.add(r1);
			sprites.add(r2);
			sprites.add(r3);
			f.draw(sprites);

			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "back.png") {
						menu();
						rankin = true;
					}
				}
			}

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void opciones() throws ClassNotFoundException, IOException, InterruptedException {
		boolean opciones = false;
		int x = 0;
		int y = 0;

		while (!opciones) {

			f.background = "menu3.png";

			x = f.getMouseX();
			y = f.getMouseY();

			ArrayList<Sprite> sprites = new ArrayList<>();
			Opciones b5 = new Opciones(1450, 175, 1700, 300, "back.png");
			sprites.add(b5);
			f.draw(sprites);

			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "back.png") {
						menu();
						opciones = true;
					}
				}
			}

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void gameOver() {
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		w.playSFX("moriste.wav");
		sprites.clear();
		f.background = "end.gif";
		f.draw(sprites);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}